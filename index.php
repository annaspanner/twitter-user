<?php
require 'vendor/autoload.php';
require_once 'twitterUser.php';

// Enter user details here to connect to Twitter API
define('CONSUMER_KEY', 'xxxxx');
define('CONSUMER_SECRET', 'xxxxx');
define('OAUTH_KEY', 'xxxxx');
define('OAUTH_SECRET', 'xxxxx');

$class = $message = '';

if (!empty($_POST)) {
    \Codebird\Codebird::setConsumerKey(CONSUMER_KEY, CONSUMER_SECRET);
    $cb = \Codebird\Codebird::getInstance();
    $cb->setToken(OAUTH_KEY, OAUTH_SECRET);
    $cb->setReturnFormat(CODEBIRD_RETURNFORMAT_ARRAY);

    $username = $_POST['username'];

    if (isValidUsername($username)) {
        $userDetails = getUserDetails($cb, $username);

        if ($userDetails['protected']) {
            $message = 'Users tweets are protected and can\'t be displayed';
            $class = 'protected';
        } else {
            $tweets = getLastFiveTweets($cb, $username);
        }
    } else {
        $message = 'Invalid Username';
        $class = 'error';
    }
}
?>

<html>
    <head>
        <title>Twitter user details</title>
        <link rel="stylesheet" type="text/css" href="twitter.css">
    </head>
    <body>
        <h1>Get Twitter user details</h1>
        <form action="" method="post" name="twitterform">
            <div class="field">
                <label for="username">Username</label>
                <input id="username" name="username" type="text" />
                <input id="submit" name="submit" type="submit" value="Get Details" />
            </div>
        </form>
        <?php
            if (!empty($userDetails)) {
                echo
                    '<div class="user">' .
                    '   <img src="' . $userDetails['image'] . '" alt="Profile Image" />' .
                    '   <h3>' . $username . '</h3>' .
                    '   <div class="counts">' .
                    '       Tweets: ' . $userDetails['tweets'] .
                    '       Followers: ' . $userDetails['followers'] .
                    '       Following: ' . $userDetails['friends'] .
                    '   </div>' .
                    '</div>';
            }
            if (!empty($tweets)) {
                echo '<div class="tweets">';
                foreach ($tweets as $tweet) {
                    if (!empty($tweet['text'])) {
                        echo
                            '<div class="tweet">' .
                                $tweet['text'] .
                            '</div>';
                    }
                }
                echo '</div>';
            }
        ?>
        <div class="message <?php print $class; ?>">
            <?php print $message; ?>
        </div>
    </body>
</html>
