<?php
function getUserDetails($cb, $username) {
    $response = $cb->users_show("screen_name=$username");

    $user = array(
        'image' => $response['profile_image_url'],
        'tweets' => $response['statuses_count'],
        'followers' => $response['followers_count'],
        'friends' => $response['friends_count'],
        'protected' => $response['protected'],
    );

    return $user;
}

function getLastFiveTweets($cb, $username) {
    $params = array(
        'screen_name' => $username,
        'count' => '5',
    );
    $response = $cb->statuses_userTimeline($params);

    return $response;
}

function isValidUsername($username) {
    return preg_match("/[a-z0-9_]+/i", $username);
}
